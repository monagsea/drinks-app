package com.example.bottomsup.di

import com.example.bottomsup.model.remote.BottomsUpService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DrinkServiceModule {

    private const val BASE_URL = "https://www.thecocktaildb.com"

    @Provides
    @Singleton
    @Named("retrofit")
    fun providesDrinkService(): BottomsUpService {
        val retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        return retrofit.create(BottomsUpService::class.java)
    }
}
