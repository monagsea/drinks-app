package com.example.bottomsup.view.drinkDetails

import com.example.bottomsup.model.response.DrinkDetailDTO

data class DetailState(
    val details: List<DrinkDetailDTO.Drink> = emptyList()
)