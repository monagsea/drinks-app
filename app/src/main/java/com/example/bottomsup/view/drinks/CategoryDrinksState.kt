package com.example.bottomsup.view.drinks

import com.example.bottomsup.model.response.CategoryDrinksDTO

data class CategoryDrinksState(
    val categories: List<CategoryDrinksDTO.Drink> = emptyList()
)
