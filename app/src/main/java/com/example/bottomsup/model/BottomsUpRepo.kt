package com.example.bottomsup.model

import com.example.bottomsup.model.remote.BottomsUpService
import com.example.bottomsup.model.response.CategoryDTO
import com.example.bottomsup.model.response.CategoryDrinksDTO
import com.example.bottomsup.model.response.DrinkDetailDTO
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Named

class BottomsUpRepo @Inject constructor(@Named("retrofit")private val service: BottomsUpService) {

    suspend fun getCategories(): CategoryDTO =
        withContext(Dispatchers.IO) {
            service.getCategories()
        }

    suspend fun getCategoryDrinks(category: String): CategoryDrinksDTO =
        withContext(Dispatchers.IO) {
            service.getCategoryDrinks(category)
        }

    suspend fun getDrinkDetails(drinkId: String): DrinkDetailDTO =
        withContext(Dispatchers.IO) {
            service.getDrinkDetails(drinkId)
        }
}