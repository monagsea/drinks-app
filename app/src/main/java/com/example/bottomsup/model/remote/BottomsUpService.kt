package com.example.bottomsup.model.remote

import com.example.bottomsup.model.response.CategoryDTO
import com.example.bottomsup.model.response.CategoryDrinksDTO
import com.example.bottomsup.model.response.DrinkDetailDTO
import retrofit2.http.GET
import retrofit2.http.Query

interface BottomsUpService {

    @GET("/api/json/v1/1/list.php")
    suspend fun getCategories(@Query("c") type: String = "list"): CategoryDTO

    @GET("/api/json/v1/1/filter.php")
    suspend fun getCategoryDrinks(@Query("c") category: String): CategoryDrinksDTO

    @GET("/api/json/v1/1/lookup.php")
    suspend fun getDrinkDetails(@Query("i") drinkId: String): DrinkDetailDTO
}