package com.example.bottomsup.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.bottomsup.model.BottomsUpRepo
import com.example.bottomsup.model.response.CategoryDrinksDTO
import com.example.bottomsup.view.drinks.CategoryDrinksState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CategoryDrinksViewModel @Inject constructor(private val repo: BottomsUpRepo) : ViewModel() {


    private val _state = MutableLiveData<CategoryDrinksState>()
    val state: LiveData<CategoryDrinksState> get() = _state

    private val _stateList = MutableLiveData<CategoryDrinksDTO>()
    val stateList: LiveData<CategoryDrinksDTO> get() = _stateList

    fun getDrinks(category: String) {
        viewModelScope.launch {
            val categoryDrinksDTO = repo.getCategoryDrinks(category)
            _state.value = CategoryDrinksState(categories = categoryDrinksDTO.drinks)
            _stateList.value = categoryDrinksDTO
        }
    }
}