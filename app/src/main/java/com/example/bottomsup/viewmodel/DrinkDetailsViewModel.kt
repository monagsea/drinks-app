package com.example.bottomsup.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.bottomsup.model.BottomsUpRepo
import com.example.bottomsup.model.response.DrinkDetailDTO
import com.example.bottomsup.view.drinkDetails.DetailState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DrinkDetailsViewModel @Inject constructor(private val repo: BottomsUpRepo) : ViewModel() {

    private val _state = MutableLiveData(DetailState())
    val state: LiveData<DetailState> get() = _state

    private val _stateList = MutableLiveData<DrinkDetailDTO>()
    val stateList: LiveData<DrinkDetailDTO> get() = _stateList


    fun getDetails(drinkId: String) {
        viewModelScope.launch {
            val drinkDetailsDTO = repo.getDrinkDetails(drinkId)
            _state.value = DetailState(details = drinkDetailsDTO.drinks)
            _stateList.value = drinkDetailsDTO
        }
    }
}