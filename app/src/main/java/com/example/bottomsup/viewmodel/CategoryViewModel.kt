package com.example.bottomsup.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.bottomsup.model.BottomsUpRepo
import com.example.bottomsup.model.response.CategoryDTO
import com.example.bottomsup.view.category.CategoryState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CategoryViewModel @Inject constructor(private val repo: BottomsUpRepo) : ViewModel() {

    private val _state = MutableLiveData(CategoryState())
    val state: LiveData<CategoryState> get() = _state

    private val _stringList = MutableLiveData<CategoryDTO>()
    val stringList: LiveData<CategoryDTO> get() = _stringList

    init {
        viewModelScope.launch {
            val categoryDTO = repo.getCategories()
            _state.value = CategoryState(categories = categoryDTO.categoryItems)
        }
    }

    fun getCategories() {
        viewModelScope.launch {
            val category = repo.getCategories()
            _stringList.value = category
        }
    }
}