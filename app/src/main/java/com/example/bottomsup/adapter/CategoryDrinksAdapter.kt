package com.example.bottomsup.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.bottomsup.databinding.ItemDrinksBinding
import com.example.bottomsup.model.response.CategoryDrinksDTO
import com.example.bottomsup.view.drinks.DrinksFragmentDirections
import com.squareup.picasso.Picasso
import dagger.hilt.android.AndroidEntryPoint

class CategoryDrinksAdapter :
    RecyclerView.Adapter<CategoryDrinksAdapter.CategoryDrinksViewHolder>() {
    private var drinks = listOf<CategoryDrinksDTO.Drink>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryDrinksViewHolder {
        return CategoryDrinksViewHolder.getInstance(parent)
    }

    override fun onBindViewHolder(holder: CategoryDrinksViewHolder, position: Int) {
        holder.bindDrink(drinks[position])
    }

    override fun getItemCount(): Int {
        return drinks.size
    }

    fun addDrinks(drinks: List<CategoryDrinksDTO.Drink>) {
        this.drinks = drinks
        notifyDataSetChanged()
    }

    class CategoryDrinksViewHolder(
        private val binding: ItemDrinksBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bindDrink(drink: CategoryDrinksDTO.Drink) = with(binding) {
            tvDrinks.text = drink.strDrink
            Picasso.get().load(drink.strDrinkThumb).into(ivDrink)
            cvDrinks.setOnClickListener {
                it.findNavController()
                    .navigate(DrinksFragmentDirections.actionDrinksFragmentToDetailFragment(drink.idDrink))
            }
        }

        companion object {
            fun getInstance(parent: ViewGroup) = ItemDrinksBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            ).let { CategoryDrinksViewHolder(it) }
        }
    }


}

