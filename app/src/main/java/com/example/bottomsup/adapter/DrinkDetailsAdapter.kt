package com.example.bottomsup.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.bottomsup.databinding.ItemDetailsBinding
import com.example.bottomsup.model.response.DrinkDetailDTO
import com.squareup.picasso.Picasso
import dagger.hilt.android.AndroidEntryPoint

class DrinkDetailsAdapter : RecyclerView.Adapter<DrinkDetailsAdapter.DrinkDetailsViewHolder>() {
    private var details = listOf<DrinkDetailDTO.Drink>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DrinkDetailsViewHolder {
        return DrinkDetailsViewHolder.getInstance(parent)
    }

    override fun onBindViewHolder(holder: DrinkDetailsViewHolder, position: Int) {
        holder.bindDrink(details[position])
    }

    override fun getItemCount(): Int {
        return details.size
    }

    fun addDetails(details: List<DrinkDetailDTO.Drink>) {
        this.details = details
        notifyDataSetChanged()
    }

    class DrinkDetailsViewHolder(
        private val binding: ItemDetailsBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bindDrink(detail: DrinkDetailDTO.Drink) = with(binding) {
            tvDrinks.text = detail.strDrink
            tvDetails.text = detail.strInstructions
            Picasso.get().load(detail.strDrinkThumb).into(ivDrink)
            if (detail.strIngredient4 != null) {
                tvIngOne.text = detail.strIngredient1
                tvIngTwo.text = detail.strIngredient2
                tvIngThree.text = detail.strIngredient3
                tvIngFour.text = detail.strIngredient4
            } else {
                binding.tvIngHeader.text = ""
            }
        }

        companion object {
            fun getInstance(parent: ViewGroup) = ItemDetailsBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            ).let { DrinkDetailsViewHolder(it) }
        }
    }
}
